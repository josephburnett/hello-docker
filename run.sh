#!/usr/bin/env bash

set -e

STEPS=$(yq -r '."hello-delegation".variables.STEPS.value' .gitlab-ci.yml) step-runner ci

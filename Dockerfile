FROM golang:1.21 as build

WORKDIR /app

COPY . ./
RUN go mod download
RUN go build -o /usr/bin/hello-delegation .

ENTRYPOINT ["/usr/bin/hello-delegation"]

package main

import (
	"flag"
	"log"
)

var (
	outputFile = flag.String("output-file", "", "")
	outputName = flag.String("output-name", "", "")
	image      = flag.String("image", "", "")
	steps      = flag.String("steps", "", "")
)

func main() {
	checkFlags()
	// connect to docker client and start a container
	// exec bridge to grpc client
	// run steps in container
	// shutdown container
	// write step results to output file
}

func checkFlags() {
	flag.Parse()
	checkFlag("output-file", outputFile)
	checkFlag("output-name", outputName)
	checkFlag("image", image)
	checkFlag("steps", steps)
}

func checkFlag(flagName string, flagValue *string) {
	if flagValue == nil && *flagValue == "" {
		log.Fatalf("flag %q is required", flagName)
	}
}
